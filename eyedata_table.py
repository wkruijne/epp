import os, re
import numpy as np
from itertools import izip
import tables as tb
import pandas as pd

from eyedata_parser import ASCParser

from IPython import embed

# create_group(self, where, name, title='', filters=None, createparents=False)
# create_table(self, where, name, description=None, title='', filters=None, 
    #expectedrows=10000, chunkshape=None, byteorder=None, createparents=False, obj=None)

class HDF_EyeDataWriter(object):
    """ HDF_Eyedata creates a big hdf table with groups for all the subjects
    """
    def __init__(self, fname, title=''):
        super(HDF_EyeDataWriter, self).__init__()
        self.fname = fname
        self.title = title
        self.ascparser = None
        return    

    def open_hdf(self):
        self.tabfile = tb.open_file(self.fname, mode='a', title=self.title)
        return

    def close_hdf(self):
        self.tabfile.close()
        return

    def add_table(self, data, type_dict=None, name='table', h5file=None):
        """
        add_table_to_hdf adds a data table to the hdf file
        """
        if h5file is None:
            h5file = self.tabfile
        if type_dict is None:
            type_dict = self.get_dtype_dict(data)
        try:
            tab = h5file.create_table(self.grp, name=name,description=type_dict,
                title='{} in file {}'.format(name, self.stem) )
        except Exception, e:
            self.close_hdf()
            raise e
        
        row = tab.row
        for r in data:
            for col in r.keys():
                row[col] = r[col]
            row.append()
        tab.flush()
        return

    def set_group(self, groupname):
        pth = '/'+groupname if not groupname.startswith('/') else groupname
        try:
            # fetch:
            self.grp = self.tabfile.get_node(pth)
        except tb.NoSuchNodeError, e:
            # create
            self.grp = self.tabfile.create_group(
                where=os.path.dirname(pth), name=os.path.basename(pth),
                title="data from {}".format(os.path.basename(pth)) ) 
        return

    def get_dtype_dict(self, data):
        """ Get data types description for columns in the table 
        assuming data is a list of homogeneous dicts
        This checks the first entry in the list
        --except for strings, where it checks the max item:
        """
        return np.dtype([(col, np.array([d[col] for d in data]).dtype)
            for col in data[0].keys()])

    ###########################
    # Storing data
    ###########################
    def store_event_data(self):
        print("Storing event data")
        self.open_hdf()
        self.set_group(self.stem)
        ap = self.ascparser
        self.add_table(data=ap.saccades, name="saccades_from_evt_file")
        self.add_table(data=ap.fixations, name="fixations_from_evt_file")
        self.add_table(data=ap.blinks, name="blinks_from_evt_file")
        ### anything else?...
        self.close_hdf()
        return

    def store_gaze_data(self):
        """
        Stores gaze data
        """
        # ap must already have been run
        assert self.ascparser is not None 
        assert self.ascparser.segments is not None
        ap = self.ascparser
        # open 
        # copy the dict, except for multidimensional columns:
        segm_data = [d.copy() for d in ap.segments]
        dcols = [s.pop('data_columns') for s in segm_data]
        try:
            gzdat = [s.pop('gz_data') for s in segm_data]
        except KeyError, e:
            ap.add_gzdat_segments()
        # assert everything is unidimensional now:
        assert not any( [np.array(segm_data[0][k]).shape 
                            for k in segm_data[0].keys()] )
        # add properties:
        print("Storing gaze data -- segment attributes")
        self.open_hdf()
        self.set_group(ap.stem)
        self.add_table(segm_data,name='segment_data')
        self.close_hdf()

        # add gaze segments:
        print("Storing gaze data -- segment data")
        # I'm just going to store it as one table -- with index marked per df
        gzsgmt_df = pd.concat([ pd.concat((
            pd.DataFrame(d['gz_data'], columns=d['data_columns']   ),
            pd.DataFrame([i]*d['gz_data'].shape[0],columns=['idx'] ) ), axis=1)
                                for i,d in enumerate(ap.segments)])
        # store it (in the right location)
        h5pth = '{}/gaze_segments'.format(self.stem)
        gzsgmt_df.to_hdf(self.fname, key=h5pth,mode='a')
        return

    def store_multidim_array(self, arr, name):
        print("Storing multidimensional data")
        self.open_hdf()
        self.set_group(self.stem)
        self.tabfile.create_array(self.grp, name, obj=arr)
        self.close_hdf()
        return
        
    #######
    # Call data processor(s)
    #######
    def run_ascparser(self, asc_fname):
        ap = self.ascparser=ASCParser(asc_fname)
        ap.run()
        self.ascparser = ap
        self.stem = ap.stem
        return

class HDF_EyeDataQuery(object):
    """class to interface with the generated HDF5"""
    def __init__(self, fname, group=None):
        super(HDF_EyeDataQuery, self).__init__()
        self.fname = fname
        self.group = group
        # useful queries for dataset
        self.queries = dict(
            valid = ("expt_phase != 'practice'"+ 
                "and correct==True and outlier==False"), 
        )
        return

    @property
    def group(self):
        return self._group
    
    @group.setter
    def group(self, value):
        if value is None:
            value='/'
        if not value.startswith('/'):
            value = '/'+value
        self._group = value
    
    def open_hdf(self):
        self.tabfile = tb.open_file(self.fname, mode='r', title=self.title)
        return

    def close_hdf(self):
        self.tabfile.close()
        return


    def get_segment_data_subset(self, query = '', filter_valid=False):
        with pd.get_store(self.fname) as store:
            table = store['{}/segment_data'.format(self.group[1:])]
        # if filter valid, first fetch only valid trials:
        if filter_valid:
            table = table.query(self.queries['valid'])
        if query:
            table = table.query(query)
        return table
    
    def get_events_in_segment(self, idx, event='saccades'):
        tabrow = self.get_segment_data_subset(query='index=={}'.format(idx) ) 
        tabname = event+'_from_evt_file'
        with pd.get_store(self.fname) as store:
            table = store['{}/{}'.format(self.group[1:],tabname)]
        return table.query('{} < start_tstamp < {}'.format(
            tabrow.start_trial_tstamp.values[0], 
             tabrow.stop_trial_tstamp.values[0]) )

    def get_multidim_array(self,name):
        self.open_hdf()
        node = tabfile.get_node( "{}/{}".format(self.group, name) )
        arr = node.read()
        self.close_hdf()
        return arr


if __name__ == '__main__':
    heye = HDF_Eyedata('ple.h5', title='Long-term Priming -- Eye Movements')
    heye.run_ascparser('ple12hk.evt')
    heye.store_event_data()
    heye.store_gaze_data()
    embed()
